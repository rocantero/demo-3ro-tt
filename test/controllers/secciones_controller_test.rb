require 'test_helper'

class SeccionesControllerTest < ActionController::TestCase
  test "should get inicio" do
    get :inicio
    assert_response :success
  end

  test "should get nosotros" do
    get :nosotros
    assert_response :success
  end

end
